FROM ubuntu:20.04

COPY crosstool /opt/crosstool/

ENV PATH "$PATH:/opt/crosstool/gcc-3.4.6-glibc-2.3.6/arm-linux/bin/"

RUN apt update && apt -y upgrade

ARG DEBIAN_FRONTEND=noninteractive

RUN apt install -y build-essential cmake gcc-multilib g++-multilib silversearcher-ag vim libtool pkg-config cmake


