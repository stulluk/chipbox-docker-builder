# chipbox-docker-builder

## Why

Chipbox satellite receiver is developed between 2009-2011. It uses an ancient toolchain (called as "crosstool" - gcc 3.4.6 - glibc 2.3.6) which sadly runs only in 32bit linux ( i386)

Therefore I decided to create a docker container that has required dependencies to run this old toolchain.

This container will be used to build various parts of the [PARS](https://gitlab.com/stulluk/chipbox-pars) ( Userspace apps of Chipbox )

## How to use

This container will be used automatically by the build scripts of [PARS](https://gitlab.com/stulluk/chipbox-pars), so you don't need to run it standalone.

But in case you are interested, you can run it as:

```bash
docker run -it -v $(pwd):/chipbox stulluk/chipbox-docker-builder /bin/bash

# and then cd to /chipbox and cd to sub directories and run ./ba or ./ca scripts

```

## How I built & push this container to dockerhub ?

```bash
docker build -t chipbox-docker-builder .
docker login
docker tag chipbox-docker-builder:latest stulluk/chipbox-docker-builder:latest
docker push stulluk/chipbox-docker-builder
```

## Contributing

PR's welcome !

