#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/opt/crosstool/gcc-3.4.6-glibc-2.3.6/arm-linux/arm-linux/lib"
XML2_LIBS="-lxml2 -lz   -liconv -lm "
XML2_INCLUDEDIR="-I/opt/crosstool/gcc-3.4.6-glibc-2.3.6/arm-linux/arm-linux/include/libxml2"
MODULE_VERSION="xml2-2.9.0"

